<meta>
	Title: Picon funguje!
	Description: Picon CMS - flat-file redakčák pro designery a malé weby
	Author: Patrik Helta
	Robots: index, follow
</meta>

## Co je ksakru Picon?

Picon je ultra-mega-hyper-rychlý redakční systém, navržený speciálně pro designery. Je tak strašně rychlý, protože je ultra jednoduchý. Je flat-file, což znamená, že nepotřebuje databázi.

Picon je mrtě jednoduchý na pochopení a používání. Je ideální pro designery a kodéry, pro návrhy prototypů a pro plně funkční weby malého až středního rozsahu, které mají převážně statický obsah.

## Design především

Picon je navržený s ohledem na designery. Nemá miliardu zbytečných funkcí, nemá složité struktury složek, nemá nespočet různých nastavení a nemá ani velký koule.

Za to má ale sexappeal. Má velmi jednoduchou strukturu složek (nemusíte se potápět do deseti úrovní podsložek) a pro uživatele má velmi rychle pochopitelné ovládání (hned budete vědět co jak funguje, aniž byste museli číst zdlouhavou dokumentaci).

**Abyste se naučili vytvářet weby v Piconu, stačí vám 15 minut!**

## Twig a Markdown

Pro pohodlí designera je PHP odděleno od HTML. K tomu slouží šablonovací systém [Twig](http://twig.sensiolabs.org/). V něm vytvoříte HTML layout.

Obsah stránky lze vytvářet pomocí [Markdown](http://daringfireball.net/projects/markdown/), takže můžete spravovat obsah webu, aniž byste psali HTML.

## Rozšiřitelnost

Picon lze rozšířit pomocí pluginů. Plugin umí přidat do redakčního systému nové funkce. To vše při zachování hyper rychlosti.

Pluginy jsou navrženy tak, aby byly mrtě jednoduché a užitečné pro designery. (*Ve skutečnosti zatím žádný plugin neexistuje.*)

Picon je vystavěný na základu [Pico](http://picocms.org/), který se již nevyvíjí. Existují i jiné vývojové větve založené na Pico, například velmi dobrý [Phile](http://philecms.com/), narozdíl od něho se Picon více soustředí na designera, než na programátora. Nehledí tak na dokonalý OOP přístup, jako spíš na rychlou pochopitelnost.