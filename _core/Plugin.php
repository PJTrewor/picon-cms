<?php
/**
 * TODO: změnit systému hooků, aby místo proměnných odkazem, předával upravené hodnoty returnem
 */
namespace Picon;

abstract class Plugin {

	/**
	 * False pokud nejde o globální plugin (výchozí), true pokud jde o globální plugin.
	 * Globální pluginy spouští všechny hooky vždy.
	 */
	protected $globalPlugin = false;

	/**
	 * @var array Konfigurace pluginu.
	 */
	protected $config = array();

	protected $pluginName = '';

	public function __construct() {
		$this->pluginName = str_replace('Picon\\', '', get_class($this));
		@include_once PLUGINS_DIR .  $this->pluginName . '/config.php';

		if (is_array($config)) {
			$this->config = $config;
			unset($config); // jakmile je zapamatováno v instanci, může se originální config zrušit
		}
	}

	/**
	 * Vrací informaci, zda je plugin globální, nebo ne.
	 *
	 * @return bool True, pokud je globální.
	 */
	public function isGlobal() {
		return $this->globalPlugin;
	}

	/**
	 * Vrací konfiguraci pluginu.
	 *
	 * @return array Konfigurace pluginu.
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * Ihned po načtení všech pluginů. Umožňuje prozkoumat názvy načtených pluginů.
	 * Provede se vždy.
	 *
	 * @param array $plugins - názvy všech pluginů
	 */
	public function pluginsLoaded($plugins) {}

	/**
	 * Ihned po načtení configu. Umožňuje config upravovat.
	 * Provede se vždy.
	 *
	 * @param array $config - upravené hodnoty configu
	 */
	public function configLoaded(&$config) {}

	/**
	 * Spustí se vždy ihned po načtení požadované URL. Umožňuje upravit požadovanou URL.
	 * @param string &$url Aktuálně požadovaná URL bez base a QS
	 */
	public function requestUrl(&$url) {}

	/**
	 * Spustí se vždy jakmile je znám MD soubor s obsahem. Umožňuje upravit cestu k souboru.
	 *
	 * @param string &$file Úplná serverová cesta k souboru (X:\www\mojesajta/_structure/pico.md)
	 */
	public function beforeLoadContent(&$file) {}

	/**
	 * Spustí se vždy ihned po načtení obsahu MD souboru. Umožňuje upravit nezparsovaný obsah MD souboru.
	 *
	 * @param string $file Úplná serverová cesta k souboru (X:\www\mojesajta/_structure/pico.md).
	 * @param string &$content Nezparsovaný originální obsah MD souboru.
	 */
	public function afterLoadContent($file, &$content) {}

	/**
	 * Spustí se před načtením 404 stránky, pouze pokud dojde ke 404.
	 * @param string file404 Úplná serverová cesta k souboru 404.
	 */
	public function beforeLoadContent404(&$file404) {}

	/**
	 * Spustí se po načtení 404 stránky, pouze pokud dojde ke 404.
	 * @param string $file404 Úplná serverová cesta k souboru 404
	 * @param string &$content Nezparsovaný originální obsah MD souboru.
	 */
	public function afterLoadContent404($file404, &$content) {}

	/*
		Spustí se vždy před parsováním obsahu aktuální stránky
		string content - nezparsovaný originální obsah MD souboru
	*/
	public function beforeParseContent(&$content) {}

	/*
		Spustí se vždy ihned po zparsování MD souboru na HTML
		array meta - zparsovaný obsah YAML front matter
		string parsedContent - zparsovaný obsah MD souboru do čistého HTML bez okolního layoutu
	*/
	public function afterParseContent(&$meta, &$parsedContent) {}

	/**
	 * Spustí se před zpracováním šablon twigu.
	 *
	 * @param string template Název výchozí šablony (index.html).
	 */
	public function beforeTwigRegister(&$template) {}

	/*
		Spustí se před vyrenderováním twig šablony.
		array twigVars - proměnné šablony ve formátu název_proměnné => hodnota_proměnné
		object twig - Twig_Environment
	*/
	public function beforeRender(&$twigVars, &$twig) {}

	/**
	 * Spustí se po vyrenderování výsledného HTML. Umožňuje upravit výsledné HTML
	 *
	 * @param &$output Výsledné HTML stránky
	 */
	public function afterRender(&$output) {}

}
?>