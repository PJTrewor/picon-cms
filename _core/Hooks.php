<?php
namespace Picon;

/**
 * Pomocná třída k volání hooků v pluginech.
 * TODO: kompletně oddělit hooky od Piconu.
 */
class Hooks {

	/**
	 * Konstanty číslující jednotlivé hooky. To nám umožní v budoucnu jednodušeji měnit pořadí a počet hooků.
	 */
	const PLUGINS_LOADED          = 1;
	const CONFIG_LOADED           = 2;
	const REQUEST_URL             = 3;
	const BEFORE_LOAD_CONTENT     = 4;
	const BEFORE_LOAD_CONTENT_404 = 5;
	const AFTER_LOAD_CONTENT_404  = 6;
	const AFTER_LOAD_CONTENT      = 7;
	const BEFORE_PARSE_CONTENT    = 8;
	const AFTER_PARSE_CONTENT     = 9;
	const BEFORE_TWIG_REGISTER    = 10;
	const BEFORE_RENDER           = 11;
	const AFTER_RENDER            = 12;

	/**
	 * @var array Převádí číslo hooku na název metody v pluginu.
	 */
	private static $functionNames = array(
			self::PLUGINS_LOADED          => 'pluginsLoaded',
			self::CONFIG_LOADED           => 'configLoaded',
			self::REQUEST_URL             => 'requestUrl',
			self::BEFORE_LOAD_CONTENT     => 'beforeLoadContent',
			self::BEFORE_LOAD_CONTENT_404 => 'beforeLoadContent404',
			self::AFTER_LOAD_CONTENT_404  => 'afterLoadContent404',
			self::AFTER_LOAD_CONTENT      => 'afterLoadContent',
			self::BEFORE_PARSE_CONTENT    => 'beforeParseContent',
			self::AFTER_PARSE_CONTENT     => 'afterParseContent',
			self::BEFORE_TWIG_REGISTER    => 'beforeTwigRegister',
			self::BEFORE_RENDER           => 'beforeRender',
			self::AFTER_RENDER            => 'afterRender'
		);

	/**
	 * Nikdy nelze vytvořit instanci.
	 */
	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	/**
	 * Vrátí název metody v pluginu, dle zadaného čísla hooku.
	 *
	 * @param int $hookId Číslo hooku.
	 * @return string Název metody v pluginu.
	 */
	public static function run($hookId) {
		return self::$functionNames[$hookId];
	}
}
?>