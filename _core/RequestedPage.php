<?php
namespace Picon;

/**
 * Třída pro zpracování aktuálně požadované stránky. Narozdíl od běžné třídy Page umožňuje lépe kontrolovat, jak bude výsledná stránka vypadat.
 */
class RequestedPage extends Page {
	/**
	 * Nastaví nový název výchozí HTML šablony.
	 *
	 * @param string $template Název nové výchozí HTML šablony.
	 */
	public function setTemplate($template) {
		$this->template = $template;
	}

	/**
	 * Vrátí obsah stránky. V závislosti na isParsed() to může být buď nezparsovaný MD kód, nebo zparsované HTML.
	 *
	 * @return string Obsah stránky.
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Nastaví nový obsah stránky.
	 *
	 * @param string $content Obsah stránky.
	 */
	public function setContent($content) {
		if (!$this->isBadRequest)
			$this->content = $content;
		else throw new Exception('Cannot change content on non-existent page:' . $this->file);
	}

	/**
	 * Zparsuje obsah souboru z MD na HTML.
	 * Zparsovaný content je poté přístupný přes metodu getContent().
	 *
	 * @return void
	 */
	public function parseContent() {
		$this->_parseContent();
	}
}

?>