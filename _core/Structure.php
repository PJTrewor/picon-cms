<?php
namespace Picon;

/**
 * Pomocná třída zpracovávající strukturu.
 */
class Structure {
	/**
	 * Nikdy nelze vytvořit instanci.
	 */
	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	/**
	 * Vrátí seznam všech složek v aktuální složce.
	 * http://www.the-art-of-web.com/php/directory-list-spl/
	 *
	 * @param string $directory Úplná serverová cesta ke složce.
	 * @param string $regex = null Regulární výraz sloužící jako filtr.
	 * @return array $directories Seznam složek.
	 */
	public static function getDirs($directory, $regex = null) {
		if($dirIterator = new \DirectoryIterator($directory)) {
			if ($regex === null)
				$fileIterator = $dirIterator;
			else
				$fileIterator = new \RegexIterator($dirIterator, $regex, \RegexIterator::MATCH);

			$directories = array();
			foreach ($fileIterator as $dirItem) {
				if ($dirItem->isDot())
					continue;
				else if ($dirItem->isDir())
					$directories[] = $dirItem->getFilename();
			}
		} else throw new \Exception("Directory $directory doesn't exist.");

		return $directories;
	}

	/**
	 * Vrátí seznam všech souborů v aktuální složce.
	 * http://www.the-art-of-web.com/php/directory-list-spl/
	 *
	 * @param string $directory Úplná serverová cesta ke složce.
	 * @param string $regex = null Regulární výraz sloužící jako filtr.
	 * @return array $files Seznam souborů.
	 */
	public static function getFiles($directory, $regex = null) {
		if($dirIterator = new \DirectoryIterator($directory)) {
			if ($regex === null)
				$fileIterator = $dirIterator;
			else
				$fileIterator = new \RegexIterator($dirIterator, $regex, \RegexIterator::MATCH);

			$files = array();
			foreach ($fileIterator as $dirItem) {
				if ($dirItem->isDot())
					continue;
				else if ($dirItem->isFile())
					$files[] = $dirItem->getFilename();
			}
		}  else throw new \Exception("Directory $directory doesn't exist.");

		return $files;
	}
}
?>