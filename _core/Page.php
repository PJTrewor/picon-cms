<?php
namespace Picon;

/**
 * Stránka Piconu.
 *
 * @author Patrik Helta
 * @license http://opensource.org/licenses/MIT
 */
class Page {

	const META_START = '<meta>';
	const META_END   = '</meta>';

	/**
	 * @var string Úplná serverová cesta k MD souboru stránky.
	 */
	protected $file = '';

	/**
	 * @var string Obsah MD souboru. V průběhu se může měnit, jak je postupně parsován.
	 */
	protected $content = null;

	/**
	 * @var bool Informace o tom, zda tato stránka opravdu existuje, nebo jde o 404.
	 */
	protected $isBadRequest = false;

	/**
	 * @var array Seznam meta značek z aktuálního MD souboru.
	 */
	protected $meta = null;

	/**
	 * @var string Naformátované datum stránky.
	 */
	protected $dateFormatted = '';

	/**
	 * @var bool Informace o tom, zda byl již zparsován $content z MD na HTML
	 */
	protected $isParsed = false;

	/**
	 * @var string Název výchozí HTML šablony. Běžně je to index.
	 */
	protected $template = null;

	/**
	 * @var string Název souboru s layoutem. Výchozí je layout.default.html.
	 */
	protected $layoutFile = null;

	/**
	 * @var array Seznam pluginů aktivovaných na této stránce.
	 */
	protected $plugins = array();

	/**
	 * Při vytvoření objektu je vždy třeba nastavit systémovou adresu k MD souboru.
	 *
	 * @param string $file Úplná systémová cesta k MD souboru.
	 */
	public function __construct($file) {
		if(file_exists($file)) {
			$this->content = file_get_contents($file);
		}
		else
			$this->isBadRequest = true;

		$this->file = $file;
	}

	/**
	 * Vrátí úplnou systémovou adresu k načtenému MD souboru.
	 *
	 * @return string Systémová adresa k souboru.
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * Vrátí informaci o tom, zda tato stránka opravdu existuje, nebo jde o 404.
	 */
	public function isBadRequest() {
		return $this->isBadRequest;
	}

	/**
	 * Vrátí meta-data z MD souboru.
	 *
	 * @return array Meta-data.
	 */
	public function getMeta() {
		if (!$this->isParsed)
			$this->_parseContent();

		return $this->meta;
	}

	/**
	 * Vrátí název výchozí HTML šablony. Běžně je to index.
	 *
	 * @return string Název výchozí HTML šablony.
	 */
	public function getTemplate() {
		if ($this->template !== null) // Nesmí již být připraveno, jinak vrátí již připravené
			return $this->template;

		if (!$this->isParsed)
			$this->_parseContent();

		return $this->template;
	}

	/**
	 * Vrátí název layoutu této stránky. Běžně je to default.
	 *
	 * @return string Název layoutu.
	 */
	public function getLayout() {
		if (!$this->isParsed)
			$this->_parseContent();

		return empty($this->meta['layout']) ? 'default' : $this->meta['layout'];
	}

	/**
	 * Vrátí název souboru layoutu této stránky. Běžně je to layout.default.html
	 *
	 * @return string Název souboru layoutu.
	 */
	public function getLayoutFile() {
		if (!$this->isParsed)
			$this->_parseContent();

		return $this->layoutFile;
	}

	/**
	 * Vrací datum aktuální stránky, pokud je nějaký datum zadaný.
	 *
	 * @param bool $formatted Pokud je true, vrací naformátovaný datum, jinak vrací původní meta značku Date. Výchozí je true.
	 * @return mixed Naformátovaný nebo nenaformátovaný datum, případně false, pokud není zadána meta značka date.
	 */
	public function getDate($formatted=true) {
		if (!$this->isParsed)
			$this->_parseContent();

		if (!isset($this->meta['date'])) // pokud není meta, vrací false
			return false;

		if ($formatted)
			return $this->dateFormatted;
		else
			return $this->meta['date'];
	}

	/**
	 * Zparsuje MD a vrátí HTML obsah stránky.
	 *
	 * @return string Obsah stránky.
	 */
	public function getContent() {
		if (!$this->isParsed)
			$this->_parseContent();

		return $this->content;
	}

	/**
	 * Vrátí seznam pluginů aktivovaných na této stránce.
	 *
	 * @return array Seznam pluginů.
	 */
	public function getPlugins() {
		if (!$this->isParsed)
			$this->_parseContent();

		return $this->plugins;
	}

	/**
	 * Vrátí úplnou URL adresu této stránky, včetně base.
	 *
	 * @return string Úplná URL adresa stránky nebo false, pokud stránka neexistuje
	 */
	public function getUrl() {
		if ($this->isBadRequest)
			return false;
		else
			return $this->fileToUrl($this->file);
	}

	/**
	 * Vrátí informaci o tom, zda byl obsah již zparsován z MD na HTML.
	 *
	 * @return bool True, pokud je obsah HTML, false pokud je původní MD.
	 */
	public function isParsed() {
		return $this->isParsed;
	}

	/**
	 * Zparsuje meta značky v MD souboru.
	 */
	protected function _parseContent()
	{
		if ($this->isParsed)
			return;
		if ($this->isBadRequest)
			throw new \Exception('Cannot read meta-data on non-existent page:' . $this->file);

		$config = Picon::getConfig();

		// Add support for custom headers by hooking into the headers array

		$contentParser = new \Mni\FrontYAML\Parser();
		$content = trim($this->content);
		$content = str_replace('%baseUrl%', $config['baseUrl'], $content); // TODO: dopsat do dokumentace
		$content = $contentParser->parse($content);
		$meta = $content->getYAML();
		$this->content = $content->getContent();

		// Připraví datum stránky
		$this->dateFormatted = empty($meta['date']) ? '' : date($config['dateFormat'], strtotime($meta['date']));

		// Připraví použitý základní template
		$this->template = empty($meta['template']) ? 'index' : $meta['template'];

		// Připraví potřebný layout
		if(empty($meta['layout']))
			$this->layoutFile = 'layout.default.html';
		else
			$this->layoutFile = 'layout.' . $meta['layout'] . '.html';

		// Připraví seznam pluginů
		if (!empty($meta['plugins'])) {
			$plugins = $meta['plugins'];
			$plugins = trim($plugins);
			$plugins = trim($plugins, ',');
			$plugins = str_replace(' ', '', $plugins);
			$plugins = explode(',', $plugins);

			$this->plugins = $plugins;
		}

		$this->meta = $meta;
		$this->isParsed = true;
	}

	/**
	 * Převede URL request na adresu k MD souboru.
	 *
	 * @param string $requestUrl URL requestu bez baseUrl a querystringu.
	 * @return string Úplná systémová cesta k MD souboru.
	 */
	public static function urlToFile($requestUrl) {
		// Znemožní přímo volat index.md, místo něj nabídne 404
		if ($requestUrl == 'index' OR preg_match('/\/index$/', $requestUrl))
			return Picon::RESPONSE_404;

		if($requestUrl)
			$file = CONTENT_DIR . $requestUrl;
		else
			$file = CONTENT_DIR .'index';

		if(is_dir($file))
			$file = CONTENT_DIR . $requestUrl .'/index'. CONTENT_EXT;
		else
			$file .= CONTENT_EXT;

		return $file;
	}

	/**
	 * Převede systémovou cestu k MD souboru na kompletní URL stránky, včetně base.
	 *
	 * @param string Systémová cesta k MD souboru.
	 * @return string Úplná URL adresa stránky, i pokud stránka neexistuje.
	 */
	public static function fileToUrl($file) {
		$config = Picon::getConfig();

		$url = str_replace(CONTENT_DIR, $config['baseUrl'], $file);
		$url = str_replace(realpath(CONTENT_DIR), $config['baseUrl'], $file);
		$url = str_replace('\\', '/', $url);
		$url = str_replace(CONTENT_EXT, '', $url);
		$url = str_replace('index', '', $url);

		return $url;
	}
}
?>