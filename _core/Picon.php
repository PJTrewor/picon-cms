<?php
namespace Picon;
require_once 'Hooks.php';
require_once 'Request.php';
require_once 'Structure.php';
require_once 'Page.php';
require_once 'RequestedPage.php';
require_once 'Plugin.php';
require_once 'composer/vendor/autoload.php';

/**
 * Picon CMS singleton. Tohle je celý CMSko. :)
 *
 * @author Patrik Helta
 * @license http://opensource.org/licenses/MIT
 * @version 0.7
 */
final class Picon {

	const RESPONSE_404 = '_404';
	const VERSION = '0.7';

	/**
	 * Instance singletonu
	 */
	private static $instance = null;

	/**
	 * Pole s pluginy, každý plugin je objekt
	 */
	private $plugins = array();

	/**
	 * Názvy globálních pluginů.
	 */
	private $globalPlugins = array();

	/**
	 * Defaultní konfigurace webu
	 */
	private $config = array(
				'siteTitle'      => 'Picon CMS',
				'baseUrl'        => null,
				'twigCache'      => true,
				'twigAutoescape' => false,
				'twigDebug'      => false
			);

	/**
	 * Interní konfigurace, které nelze uživatelsky měnit, ale v případě potřeby je můžeš přepsat přímo tady ;)
	 */
	private $internalConfig = array(
				'twigAutoreload' => true
			);

	/**
	 * @var arary Meta značky aktuální stránky
	 */
	private $meta = null;

	/**
	 * @var array Proměnné šablony Twig aktuální stránky
	 */
	private $twigVars = array();

	/**
	 * @var object Twig objekt aktuální stránky
	 */
	private $twig = null;

	/**
	 * @var object Page objekt aktuální stránky
	 */
	private $actualPage = null;

	/**
	 * @var object Objekt s aktuálním requestem
	 */
	private $request = null;

	/**
	 * @var string HTML výsledné aktuální stránky
	 */
	private $OUTPUT = null;

	/**
	 * Singleton
	 */
	private function __construct() {}
	private function __clone() {}
	private function __wakeup() {}

	/**
	 * Spustí Picon. Tady začínáme. Spustit ho lze pouze jednou.
	 */
	static public function run() {
		if (is_null(static::$instance)) {
			self::$instance = new Picon();
		}
		else throw new Exception('Picon is initialized already.');

		self::$instance->_init();         // inicializuje základní součásti
		self::$instance->_actualPage();   // připraví vše potřebné k zobrazení aktuální stránky
		self::$instance->_renderOutput(); // vyrenderuje výsledné HTML

		echo self::$instance->OUTPUT;     // zobrazí web a vrátí řízení zpět do index.php
	}

	/**
	 * Inicializuje základní součásti Piconu - config a pluginy.
	 */
	private function _init() {
		$this->_onInitLoadPlugins();    // nahraje pluginy
		$this->_onInitUserConfig();     // nahraje config.php
		$this->_onInitParseRequest();   // zparsuje URL
	}

	/**
	 * Nahraje všechny pluginy
	 */
	private function _onInitLoadPlugins() {
		$this->plugins = array();
		$plugins = Structure::getDirs(PLUGINS_DIR, "/^[^.]/"); // tečkované pluginy budou vynechány

		if(!empty($plugins)) {
			foreach($plugins as $pluginName) {

				$pluginFile = PLUGINS_DIR . $pluginName . '/plugin.php';
				if (file_exists($pluginFile)) {

					include_once($pluginFile);
					$pluginClassName = "\Picon\\" . $pluginName; // Class včetně namespace

					if(class_exists($pluginClassName) AND is_subclass_of($pluginClassName, '\Picon\Plugin')) {
						$pluginObject = new $pluginClassName;
						$this->plugins[$pluginName] = $pluginObject;
						if ($pluginObject->isGlobal()) // uloží názvy globálních pluginů
							$this->globalPlugins[] = $pluginName;
					} else throw new \Exception('Plugin class name in ' . $pluginName . " must have the same name as plugin folder and must be extended \Picon\Plugin class.");

				} else throw new \Exception('Plugin ' . $pluginName . " doesn't have plugin.php file.");
			}
		}

		$this->_runHooks(Hooks::PLUGINS_LOADED, array($plugins));
	}

	/**
	 * Nahraje konfiguraci CMS z config.php, jinak zůstanou defaultní hodnoty
	 */
	private function _onInitUserConfig() {
		@include_once(ROOT_DIR .'config.php'); // nezobrazí chybu, pokud config.php neexistuje
		if (is_array($config))
			$config = array_merge($this->config, $config);

		if (!empty($this->config['baseUrl']))
			$config['baseUrl'] = rtrim($config['baseUrl'], '/'); // odstraní lomítko na konci uživatelské baseUrl

		$this->_runHooks(Hooks::CONFIG_LOADED, array(&$config));
		$this->config = $config;
		unset($config); // jakmile je zapamatováno v instanci, může se originální config zrušit
	}

	/**
	 * Z URL requestu nastaví různé hodnoty requestu potřebné pro správný chod systému
	 */
	private function _onInitParseRequest() {
		$this->request = new Request(); // skutečný request

		if (empty($this->config['baseUrl'])) { // nastaví baseUrl, pokud není již nastaveno configem
			$url = trim($this->request->getRequestUrl(), '/');
			$url = str_replace($url, '', $this->request->getFullUrl());
			$this->config['baseUrl'] = rtrim($url, '/');
		}
	}

	/**
	 * Připraví vše potřebné pro vyrenderování aktuální stránky.
	 * 1) Vytvoří stránku
	 * 2) Ověří, zda není 404
	 * 3) ZRUŠENO
	 * 4) Zparsuje content
	 * 5) Nahraje Twig
	 * 6) Připraví twigVars
	 * Vše k renderování je na konci připraveno (ale nerenderuje).
	 * TODO: převést na samstaný objekt.
	 */
	private function _actualPage() {
		$url = $this->request->getRequestPage();
		$this->_runHooks(Hooks::REQUEST_URL, array(&$url));
		$file = Page::urlToFile($url);

		// 1) Vytvoří stránku
		$this->_runHooks(Hooks::BEFORE_LOAD_CONTENT, array(&$file));

		if ($file === self::RESPONSE_404) {
			$pluginsBadRequest = true;
		} else {
			$this->actualPage = new \Picon\RequestedPage($file);
			$pluginsBadRequest = false;
		}

		// 2) Ověří, zda není 404
		if($pluginsBadRequest OR $this->actualPage->isBadRequest()) { // Pokud chceme načíst stránku, která nemá svůj MD soubor, nebo načítáme přímo index
			$content = $this->_setResponse404();
		} else
			$content = $this->actualPage->getContent(); // načte content, pokud není 404

		$actualPage =& $this->actualPage; // Použijeme zkrácený zápis
		$this->_runHooks(Hooks::AFTER_LOAD_CONTENT, array($actualPage->getFile(), &$content));

		// 3) Načte meta
		//$this->_runHooks(Hooks::AFTER_READ_META, array(&$meta));

		// 4) Zparsuje content
		$this->_runHooks(Hooks::BEFORE_PARSE_CONTENT, array(&$content));
		$actualPage->setContent($content);
		$actualPage->parseContent();

		$meta = $actualPage->getMeta();
		$content = $actualPage->getContent();
		$this->_runHooks(Hooks::AFTER_PARSE_CONTENT, array(&$meta, &$content));
		$this->meta = $meta;
		$actualPage->setContent($content);
		unset($content); // content už nebude potřeba

		// 5) Nahraje Twig
		$template = $actualPage->getTemplate();
		$this->_runHooks(Hooks::BEFORE_TWIG_REGISTER, array(&$template));
		$actualPage->setTemplate($template);
		$this->_registerTwig();

		// 6) Připraví vyrenderování výsledného HTML
		$twigVars = $this->twigVars;
		$twig = $this->twig;
		$this->_runHooks(Hooks::BEFORE_RENDER, array(&$twigVars, &$twig));
		$this->twigVars = $twigVars;
		$this->twig = $twig;
	}

	/**
	 * Vyrenderuje HTML aktuální stránky
	 */
	private function _renderOutput() {
		$output = $this->twig->render($this->actualPage->getTemplate() .'.html', $this->twigVars);
		$this->_runHooks(Hooks::AFTER_RENDER, array(&$output));
		$this->OUTPUT = $output;
	}

	/**
	 * Zaregistruje potřebné knihovny pro vyrenderování šabony Twig.
	 * Zaregistruje proměnné twigVars.
	 */
	private function _registerTwig() {
		$loader = new \Twig_Loader_Filesystem(THEMES_DIR);
		$actualPage = $this->actualPage;

		$twigConfig = array(
			'debug' => $this->config['twigDebug'],
			'cache' => ($this->config['twigCache']) ? CACHE_DIR : false,
			'autoescape' => $this->config['twigAutoescape'],
			'auto_reload' => $this->internalConfig['twigAutoreload']
			);
		$this->twig = new \Twig_Environment($loader, $twigConfig);
		$this->twig->addExtension(new \Twig_Extension_Debug());

		$this->twigVars = array(
			'baseDir'     => rtrim(ROOT_DIR, '/'),
			'baseUrl'     => $this->config['baseUrl'],
			'request'     => $this->request->getAll(),
			'requestFile' => $actualPage->getFile(),
			'layout'      => $actualPage->getLayout(),
			'layoutFile'  => $actualPage->getLayoutFile(),
			'template'    => $actualPage->getTemplate(),
			'plugins'     => $actualPage->getPlugins(),
			'date'        => $actualPage->getDate(),
			'meta'        => $this->meta,
			'homepage'    => $this->request->getRequestPage() == '/' ? true : false,
			'siteTitle'   => $this->config['siteTitle'],
			'themeDir'    => THEMES_DIR,
			'config'      => $this->config,
			'content'     => $actualPage->getContent()
		);
	}

	/**
	 * Zpracuje hook a spustí ho u všech relevantních pluginů.
	 *
	 * @param int $hookId Číslo hooku ke spuštění
	 * @param array $args Volitelné argumenty
	 */
	private function _runHooks($hookId, $args = array())
	{
		if (!empty($args) AND !is_array($args))
			throw new Exception('Hook arguments must be an array.');

		if(!empty($this->plugins)) {

			if ($hookId >= Hooks::AFTER_PARSE_CONTENT) { // spustí hook pouze u globálních a zaregistrovaných pluginů
				$pluginsToRun = $this->globalPlugins;
				$pluginsToRun = array_merge($pluginsToRun, $this->actualPage->getPlugins());
			} else { // spustí hook u všech pluginů
				$pluginsToRun = array_keys($this->plugins);
			}

			foreach ($pluginsToRun as $pluginName) { // Projde všechny spustitelné pluginy
				if (empty($this->plugins[$pluginName]))
					throw new \Exception("Plugin $pluginName doesn't exist.");

				$plugin = $this->plugins[$pluginName]; // Aktuální objekt pluginu
				$callableFunction = array($plugin, Hooks::run($hookId));

				if (is_callable($callableFunction))
					call_user_func_array($callableFunction, $args);
			}
		}
	}

	/**
	 * Vrátí názvy načtených pluginů.
	 * @return array Názvy načtených pluginů
	 */
	public static function getPlugins() {

		return array_keys(self::$instance->plugins);
	}

	/**
	 * Vrátí instanci zadaného pluginu.
	 * @return object Instance třídy Plugin
	 */
	public static function getPluginInstance($pluginName) {

		return self::$instance->plugins[$pluginName];
	}

	/**
	 * Vrátí aktuální konfiguraci.
	 * @return array Konfigurace
	 */
	public static function getConfig() {

		return self::$instance->config;
	}

	/**
	 * Vrátí objekt s aktuálním requestem.
	 *
	 * @return object Aktuální request
	 */
	public static function currentRequest() {
		return self::$instance->request;
	}

	/**
	 * Nastaví stav odpovědi pro uživatele na 404 a vrátí obsah 404 stránky.
	 *
	 * @return string Obsah souboru 404.
	 */
	private function _setResponse404() {
		$file404 = CONTENT_DIR . self::RESPONSE_404 . CONTENT_EXT;
		$this->_runHooks(Hooks::BEFORE_LOAD_CONTENT_404, array(&$file404));

		$this->actualPage = new \Picon\RequestedPage($file404);
		if ($this->actualPage->isBadRequest())
			throw new \Exception('Missing _404.md file.');

		header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
		$content = $this->actualPage->getContent();
		$this->_runHooks(Hooks::AFTER_LOAD_CONTENT_404, array($file404, &$content));
		$this->actualPage->setContent($content);

		return $content;
	}
}