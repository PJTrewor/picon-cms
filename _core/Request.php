<?php
namespace Picon;

class Request {

	private $serverData = array();

	/**
	 * Kompletní cesta, bez base, bez QS
	 */
	private $requestUrl = null;

	/**
	 * Kompletní cesta, bez base, včetně QS
	 */
	private $requestPage = null;

	/**
	 * Kompletní cesta, včetně base, včetně QS
	 */
	private $fullUrl = null;

	/**
	 * @var (bool) Zda již nylo zparsováno.
	 */
	private $isParsed = false;

	public function __construct($http_host = null, $php_self = null, $request_uri = null) {
		if ($http_host === null)
			$this->serverData['HTTP_HOST'] =  isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''; // Doména (localhost)
		else
			$this->serverData['HTTP_HOST'] = $http_host;

		if ($request_uri === null)
			$this->serverData['REQUEST_URI'] =  isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : ''; // Vše za doménou (/picon/)
		else
			$this->serverData['REQUEST_URI'] = $request_uri;

		if ($php_self === null)
			$this->serverData['PHP_SELF'] =  isset($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : ''; // Cesta k indexu z rootu (/picon/index.php)
		else
			$this->serverData['PHP_SELF'] = $php_self;
	}

	private function _parseRequest() {
		if ($this->isParsed) // zprasuje se pouze jednou
			return;

		$config = Picon::getConfig();

		// ====== Nejprve sestavíme cestu k index.php, abychom věděli, zda homepage odpovídá rootu složek, nebo je homepage v podsložce
		if (!empty($config['baseUrl'])) { // pokud je base zadáno v configu, bude se brát jeho adresa jako výchozí
			$indexUrl  = $config['baseUrl'];
			$indexUrl  = preg_replace('/https?:\/\//', '', $indexUrl); // odstraní http://
			$indexUrl .= '/index.php';
			$indexUrl  = str_replace($this->serverData['HTTP_HOST'], '', $indexUrl); // v podstatě vytvoří stejný string, jaký vrací PHP_SELF
		} else
			$indexUrl  = $this->serverData['PHP_SELF'];

		if (isset($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] != 'off')
			$protocol = 'https://';
		else
			$protocol = 'http://';

		$url = '';
		$requestUri = $this->serverData['REQUEST_URI'];

		// ====== Nyní zjistíme, jaká podstránka vůči homepage je žádáná
		if ($requestUri != $indexUrl) { // pokud jsou tyhle dvě hodnoty stejné, chce uživatel přímo index.php
			$url = str_replace('/index.php', '', $indexUrl); // index.php nikdy nebude souřástí platné url
			$url = '/'. str_replace('/', '\/', $url) .'/'; // převod na regulár
			$url = preg_replace($url, '', $requestUri, 1); // zde se nahrazuje, pokud není homepage v rootu složek
		} else {
			// pokud žádáme přímo root index.php, musíme Piconu podstrčit soubor jako by to byla neexistující složka a tím se zobrazí 404
			// toto nastane jen tehdy, pokud někdo zadá do URL přímo adresu k index.php
			$url = 'index.php';
		}

		$this->requestUrl = $url;
		$this->requestPage = preg_replace('/\/*\?.*/', '', $url); // ustřihne query string
		$this->fullUrl = $protocol . $this->serverData['HTTP_HOST'] . $requestUri;

		$this->isParsed = true;
	}

	public function getAll() {
		$this->_parseRequest();

		return array(
			'requestUrl' => $this->requestUrl,
			'requestPage' => $this->requestPage,
			'fullUrl' => $this->fullUrl
		);
	}

	/**
	 * Úplná cesta k podstránce, tj. URL bez base, vč. QS.
	 */
	public function getRequestUrl() {
		$this->_parseRequest();

		return $this->requestUrl;
	}

	/**
	 * Cesta k podstránce, tj. URL bez base a bez QS.
	 */
	public function getRequestPage() {
		$this->_parseRequest();

		return $this->requestPage;
	}

	public function getFullUrl() {
		return $this->fullUrl;
	}
}

?>