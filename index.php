<?php
/*
	What's this?
	This is Picon CMS, heavily customized Pico CMS v0.8.
	Original CMS can be found on http://picocms.org
	Author: Patrik Helta
	Pico by: Gilbert Pellegrom
	Version: 0.7
*/
define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('CONTENT_EXT', '.md');
define('CONTENT_DIR', ROOT_DIR .'_structure/');
define('LIB_DIR',     ROOT_DIR .'_core/');
define('PLUGINS_DIR', ROOT_DIR .'_plugins/');
define('THEMES_DIR',  ROOT_DIR .'_templates/');
define('CACHE_DIR',   LIB_DIR  .'cache/');

require_once(LIB_DIR .'Picon.php');
try {
	\Picon\Picon::run();
} catch (Exception $e) {
	header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
	echo '<p><strong>' . $e->getMessage() . '</strong></p><br>';
	echo "<pre>$e</pre>";
	die;
}
?>