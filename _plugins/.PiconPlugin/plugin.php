<?php
namespace Picon;
/**
 * Příklad pluginu pro Picon. Doplní nad obsah každé stránky žlutý box s textem zadaným v configu.
 *
 * @author Patrik Helta
 * @license http://opensource.org/licenses/MIT
 */
class PiconPlugin extends Plugin {

	protected $globalPlugin = true;

	public function afterParseContent(&$meta, &$parsedContent) {
		$newContent  = '<div style="padding: 30px; background-color: #ff0; font-size: 3em;"> ';
		$newContent .= $this->config['text'];
		$newContent .= '</div>';

		$parsedContent = $newContent . $parsedContent;
	}
}

?>
